#ifndef SUBMARINE_H
#define SUBMARINE_H

#include <Ship.h>


class Submarine : public Ship
{
    private:

    protected:

    public:
        Submarine();
        virtual ~Submarine();

        std::vector<Coord> whatIsHit(Coord cd);
        bool fire(Ship* s, std::string coord);

};

#endif // SUBMARINE_H
