#ifndef COORD_H
#define COORD_H

#include <cstdlib>
#include <cstdio>
#include <ctime>

class Coord
{
    private:
        int m_x;
        int m_y;

    protected:

    public:
        Coord();
        Coord(int _x, int _y);
        virtual ~Coord();

        int getX() { return m_x; }
        void setX( int val) { m_x = val; }
        int getY() { return m_y; }
        void setY(int val) { m_y = val; }


};

#endif // COORD_H
