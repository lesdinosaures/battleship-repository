#ifndef GRID_H
#define GRID_H

#include <string>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string>

#include <windows.h>

#include "Player.h"
#include "Ship.h"
#include "Battleship.h"
#include "Destroyer.h"
#include "Cruiser.h"
#include "Submarine.h"
#include "Coord.h"

class Grid
{
    private:

    protected:

    public:
        Grid();
        virtual ~Grid();

        void update(Player& p1, Player& p2);
        void draw(Player& p1, Player& p2, int posX = 0, int posY = 0);
        void goTo(int lig, int col);
        int getRang(Player& p, int x, int y);
        void drawActions(short choice);
        void putSpace(int spaceNumber);
        void writeStringWithCol(std::string text, int col, int bg);

        void generateShips(Player& p);
        void createShip(Ship* s, int length);
        bool collision(Player& p, unsigned int x);
        bool collisionShip(Player& p, Coord cd, unsigned int x);
        bool collisionGrid(Coord cd);

};

#endif // GRID_H
