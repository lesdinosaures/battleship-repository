#ifndef SHIP_H
#define SHIP_H

#include <string>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <ctime>
#include "Coord.h"


class Ship
{
    private:

        unsigned int m_direction;

    protected:

        unsigned int m_size;
        unsigned int m_firePower;
        char m_displayChar;

    public:
        std::vector<Coord> cases;
        std::vector<bool> isHit;

        Ship();
        virtual ~Ship();

        unsigned int getDirection() { return m_direction; }
        void setDirection(unsigned int val) { m_direction = val; }
        unsigned int getSize() { return m_size; }
        void setSize(unsigned int val) { m_size = val; }
        unsigned int getFirePower() { return m_firePower; }
        void setFirePower(unsigned int val) { m_firePower = val; }
        char getDisplayChar() { return m_displayChar; }

        //Fait avancer un bateau. Les coordonn�es sont modifi�es dans la classe Ship
        //+[IN] s: le bateau � faire avancer (s!=NULL)
        //+[IN] Cardinal c(a cr�er l'enum Cardinal): direction dans laquelle se trouve le bateau
        //+[OUT] bool ok: si le bateau a pu avancer sans collision
        void moveForward();

        //Fait tourner un bateau. Les coordonn�es sont modifi�es dans la classe Ship
        //+[IN] s: le bateau � faire tourner (s!=NULL)
        //+[IN] Cardinal c: direction dans laquelle se trouve le bateau
        //+[IN] direction: direction dans laquelle on veut tourner le bateau (droite ou gauche)
        //+[IN] wait: bool�en permettant de faire tourner les battleship en 2 tours
        //+[OUT] bool ok: si le bateau a pu tourner sans collision
        void turn();

        
        //Modifie le bateau (le vecteur de bool isHit) si le bateau a �t� touch� par un tir ennemi
        //Recoit en param�tre la coordonn�e du tir r�usii ennemi
        void wasShot(Coord cd);

        //Retourne la zone d'effet du tir en fonction du type de bateau
        virtual std::vector<Coord> whatIsHit(Coord cd) = 0;

        bool isShipSunk();


        //Actualise ne fonction des deux flottes des deux joueurs respectivement
        //+[IN] fleet1, fleet2: les flots de navires de chaque joueur
        //+[OUT] bool ok: return true si la grlle a �t� g�n�r�e avec succ�s, false sinon
        bool setGrid(std::vector<Ship*> fleet1, std::vector<Ship*> fleet2);




};

#endif // SHIP_H
