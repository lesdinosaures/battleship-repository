#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <windows.h>
#include "Ship.h"

class Player
{
    private:



    protected:

    public:
        Player();
        virtual ~Player();

        std::vector<Ship*> m_fleet;

        bool ableToMove (int rang);
        bool ableToTurn (int rang);

        //Vérifie si le tir ennemi touche une ou plusieurs cases de bateau. Si c'est la cas, on passe l'information au bateau concerné via la fonction wasShot de Ship et on retourne true.
        //Recoit en paramètre toutes les coordonnées des cases touchées par le tir ennemi.
        bool isShotSuccessful (std::vector<Coord> hitBox);
        
        bool collis(int rang, int x, int y);
        bool collisGrid(int x, int y);
        bool collisShip(int rang, int x, int y);


};

#endif // PLAYER_H
