#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include "Player.h"
#include "Grid.h"

class Menu
{
    private :
        int m_choice;        // pour le menu
        int m_choice2;      // pour l'aide
        bool m_quit;
        bool m_revenir_menu;

    public :
        Menu();
        ~Menu();

        // Launching a new game
        void launchNewGame();

        // Loading a previous game
        void loadGame();

        // Loop
        void gameLoop(Player& p1, Player& p2, Grid g);

        // Shows menu
        int show();

        // Shows instructions' menu
        void show2();
        void showHelp();        // R�gles
        void showCommands();    // Commandes

        bool getQ() { return m_quit; }

};

#endif // MENU_H_INCLUDED
