#ifndef DESTROYER_H
#define DESTROYER_H

#include <Ship.h>


class Destroyer : public Ship
{
    private:
        bool m_isRocketFired;

    protected:

    public:
        Destroyer();
        virtual ~Destroyer();

        bool getIsRocketFired() { return m_isRocketFired; }
        void setIsRocketFired(bool val) { m_isRocketFired = val; }

        std::vector<Coord> whatIsHit(Coord cd);
        bool fire(Ship* s, std::string coord);

};

#endif // DESTROYER_H
