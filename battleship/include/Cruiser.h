#ifndef CRUISER_H
#define CRUISER_H

#include <Ship.h>


class Cruiser : public Ship
{
    private:

    protected:

    public:
        Cruiser();
        virtual ~Cruiser();

        std::vector<Coord> whatIsHit(Coord cd);
        bool fire(Ship* s, std::string coord);

};

#endif // CRUISER_H
