#ifndef BATTLESHIP_H
#define BATTLESHIP_H

#include <Ship.h>


class Battleship : public Ship
{
    private:

    protected:

    public:
        Battleship();
        virtual ~Battleship();


        std::vector<Coord> whatIsHit(Coord cd);
        bool fire(Ship* s, std::string coord);

};

#endif // BATTLESHIP_H
