#include "Ship.h"

Ship::Ship()
{
    //ctor
}

Ship::~Ship()
{
    //dtor
}

void Ship::moveForward()
{
    switch(getDirection())
    {
        case 0:
            for (size_t i = 0 ; i < cases.size() ; i++)
            {
                cases[i].setX(cases[i].getX() - 1);
            }
            break;
        case 1:
            for (size_t i = 0 ; i < cases.size() ; i++)
            {
                cases[i].setX(cases[i].getX() + 1);
            }
            break;
        case 2:
            for (size_t i = 0 ; i < cases.size() ; i++)
            {
                cases[i].setY(cases[i].getY() + 1);
            }
            break;
        case 3:
            for (size_t i = 0 ; i < cases.size() ; i++)
            {
                cases[i].setY(cases[i].getY() - 1);
            }
            break;
    }
}

void Ship::turn()
{
    if ((getDirection() == 0) || (getDirection() == 1))
    {
        for (size_t i = 1 ; i < cases.size() ; i++) //On commence � 1 car pas besoin de tester la case centrale
        {
            if ((i = 1))
            {
                cases[i].setX(cases[i].getX() + 1);
                cases[i].setY(cases[i].getY() + 1);
            }
            if ((i = 2))
            {
                cases[i].setX(cases[i].getX() - 1);
                cases[i].setY(cases[i].getY() - 1);
            }
            if ((i = 3))
            {
                cases[i].setX(cases[i].getX() + 2);
                cases[i].setY(cases[i].getY() + 2);
            }
            if ((i = 4))
            {
                cases[i].setX(cases[i].getX() - 2);
                cases[i].setY(cases[i].getY() - 2);
            }
            if ((i = 5))
            {
                cases[i].setX(cases[i].getX() + 3);
                cases[i].setY(cases[i].getY() + 3);
            }
            if ((i = 6))
            {
                cases[i].setX(cases[i].getX() - 3);
                cases[i].setY(cases[i].getY() - 3);
            }
        }

    }
    else if ((getDirection() == 2) || (getDirection() == 3))
    {
        for (size_t i = 1 ; i < cases.size() ; i++) //On commence � 1 car pas besoin de tester la case centrale
        {
            if ((i == 1))
            {
                cases[i].setX(cases[i].getX() - 1);
                cases[i].setY(cases[i].getY() + 1);
            }
            if ((i == 2))
            {
                cases[i].setX(cases[i].getX() + 1);
                cases[i].setY(cases[i].getY() - 1);
            }
            if ((i == 3))
            {
                cases[i].setX(cases[i].getX() - 2);
                cases[i].setY(cases[i].getY() + 2);
            }
            if ((i == 4))
            {
                cases[i].setX(cases[i].getX() + 2);
                cases[i].setY(cases[i].getY() - 2);
            }
            if ((i == 5))
            {
                cases[i].setX(cases[i].getX() - 3);
                cases[i].setY(cases[i].getY() + 3);
            }
            if ((i == 6))
            {
                cases[i].setX(cases[i].getX() + 3);
                cases[i].setY(cases[i].getY() - 3);
            }
        }
    }
}

void Ship::wasShot(Coord cd)
{
    for (size_t i = 0 ; i < cases.size() ; i++)
    {
        if ((cases[i].getX() == cd.getX()) && (cases[i].getY() == cd.getY()))
        {
            isHit[i] = true;
        }
    }
}

bool Ship::isShipSunk()
{
    for (size_t i = 0 ; i < isHit.size() ; i++)
    {
        if (!isHit[i])
            return false;
    }
    return true;
}

