#include "Grid.h"

Grid::Grid()
{

}

Grid::~Grid()
{
    //dtor
}

void Grid::writeStringWithCol(std::string text, int col, int bg){
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    // col = color
    // bg = background color
    SetConsoleTextAttribute(hConsole, 16 * bg + col);
    std::cout << text;

    // Go back to normal color
    SetConsoleTextAttribute(hConsole, 16 * 0 + 15);
}

void Grid::putSpace(int spaceNumber){
    std::string text("");
    for(short i = 0; i < spaceNumber; i++){
        text += " ";
    }
    std::cout << text;
}

void Grid::drawActions(short choice){
    // Space numbers
    int initialSpace(5);
    int otherSpace(5);

    // Action strings
    std::string actions[] = {"SHOOT the ennemy", "MOVE a ship", "TURN a ship"};

    // Go after the grids
    goTo(34, 0);

    for(short a = 0; a < 3; a++){
        (a == 0 ? putSpace(initialSpace) : putSpace(otherSpace));
        if(a == choice)
            writeStringWithCol(actions[a], 9, 15);
        else
            writeStringWithCol(actions[a], 15, 0);
    }
}

int Grid::getRang(Player &p, int x, int y) {
    int trueX(0);
    int trueY(0);
    for(size_t f = 0; f < p.m_fleet.size(); f++){
        for(size_t g = 0; g < p.m_fleet[f]->cases.size(); g++){
            trueX = (x/2);
            trueY = (y/3);

            if(p.m_fleet[f]->cases[g].getX() == trueX && p.m_fleet[f]->cases[g].getY() == trueY){
                // La case g du bateau f
                //goTo(100, 0);
                //std::cout << f;
                return f;
            }
        }
    }
    return -1;
}

void Grid::update(Player& p1, Player& p2){
    int x(0);
    int y(0);

    int rang(-1);
    char xTir;
    int yTir;

    // Y location of ennemy empty grid
    int secondGridY(60);

    int taille(15);
    bool updatePos(false);
    short choice(0);

    bool exit = false;

    // Draw grids first time
    this->draw(p1, p2);
    this->draw(p1, p2, 0, secondGridY);

    // Main loop
    while(!GetAsyncKeyState(VK_ESCAPE) && (!exit)){

        // Move on the grid
        if(GetAsyncKeyState(VK_RIGHT) && y < 3*(taille-1)){
            y+=3;
            while(GetAsyncKeyState(VK_RIGHT));
        }
        if(GetAsyncKeyState(VK_LEFT) && y > 0){
            y-=3;
            while(GetAsyncKeyState(VK_LEFT));
        }
        if(GetAsyncKeyState(VK_UP) && x > 0){
            x-=2;
            while(GetAsyncKeyState(VK_UP));
        }
        if(GetAsyncKeyState(VK_DOWN) && x < 2*(taille-1)){
            x+=2;
            while(GetAsyncKeyState(VK_DOWN));
        }

        rang = getRang(p1, x, y);

        // Chose action
        if(GetAsyncKeyState(VK_SPACE) && rang >= 0){
            // Draw actions first time
            while(GetAsyncKeyState(VK_SPACE));
            drawActions(choice);

            // While action not chosen,
            // you can move through all possible actions
            while(!GetAsyncKeyState(VK_SPACE)){
                updatePos = false;

                if(GetAsyncKeyState(VK_RIGHT)){
                    choice = (choice == 2 ? 0 : choice + 1);
                    updatePos = true;
                    while(GetAsyncKeyState(VK_RIGHT));
                }
                if(GetAsyncKeyState(VK_LEFT)){
                    choice = (choice == 0 ? 2 : choice - 1);
                    updatePos = true;
                    while(GetAsyncKeyState(VK_LEFT));
                }

                // If arrow left or right, update action choice
                if(updatePos){
                    drawActions(choice);
                }

                // If want to select another space
                if(GetAsyncKeyState(VK_ESCAPE)){
                    // Get back to movment
                    break;
                }
            }
            if(!GetAsyncKeyState(VK_ESCAPE)){
                while(GetAsyncKeyState(VK_SPACE));
                // Action was chosen

                switch(choice){
                case 0:
                    goTo(40,0);
                    std::cout << "Entrez la coordonn�es x de votre tir :";

                    std::cin >> xTir;
                    std::cout << std::endl << xTir;
                    std::cout << std::endl;
                    std::cout << "Entrez la coordonn�es y de votre tir :";
                    std::cin >> yTir;
                    p2.isShotSuccessful(p1.m_fleet[rang]->whatIsHit(coordConvert(xTir, yTir)));
                    draw(p1,p2);
                    goTo(38,0);
                    std::cout << "Appuyer sur espace pour passer au tour de votre adversaire";
                    while(!GetAsyncKeyState(VK_SPACE));
                    while(GetAsyncKeyState(VK_SPACE));
                    std::cout << "                                                       ";
                    break;
                case 1:
                    // Moves the boat
                    if(!p1.ableToMove(rang)){
                        // Error, cannot move
                    }
                    else{
                        draw(p1, p2);
                        goTo(38,0);
                        std::cout << "Appuyer sur espace pour passer au tour de votre adversaire";
                    }
                    while(!GetAsyncKeyState(VK_SPACE));
                    while(GetAsyncKeyState(VK_SPACE));
                    std::cout << "                                                       ";
                    break;
                case 2:
                    // Turns the boat
                    if(!p1.ableToTurn(rang)){
                        // Error, cannot turn
                    }
                    else{
                        draw(p1, p2);
                        goTo(38,0);
                        std::cout << "Appuyer sur espace pour passer au tour de votre adversaire";
                    }
                    while(!GetAsyncKeyState(VK_SPACE));
                    while(GetAsyncKeyState(VK_SPACE));
                    std::cout << "                                                       ";
                    break;
                }
                exit = true;
            }
            else{
                while(GetAsyncKeyState(VK_ESCAPE));
            }
        }
        goTo(x + 2, y + 3);
    }
}

Coord Grid::coordConvert(char xTir, int yTir)
{
    switch(xTir)
    {
    case 'a':
        return Coord(0,yTir);
        break;
    case 'b':
        return Coord(1,yTir);
        break;
    case 'c':
        return Coord(2,yTir);
        break;
    case 'd':
        return Coord(3,yTir);
        break;
    case 'e':
        return Coord(4,yTir);
        break;
    case 'f':
        return Coord(5,yTir);
        break;
    case 'g':
        return Coord(6,yTir);
        break;
    case 'h':
        return Coord(7,yTir);
        break;
    case 'i':
        return Coord(8,yTir);
        break;
    case 'j':
        return Coord(9,yTir);
        break;
    case 'k':
        return Coord(10,yTir);
        break;
    case 'l':
        return Coord(11,yTir);
        break;
    case 'm':
        return Coord(12,yTir);
        break;
    case 'n':
        return Coord(13,yTir);
        break;
    case 'o':
        return Coord(14,yTir);
        break;
    }
}

// Going to a terminal position
void Grid::goTo(int lig, int col)
{
    COORD mycoord;

    mycoord.X = col;
    mycoord.Y = lig;
    SetConsoleCursorPosition(GetStdHandle( STD_OUTPUT_HANDLE ), mycoord);
}

// Drawing the grid on the terminal
void Grid::draw(Player& p1, Player& p2, int posX, int posY) {

    int taille(15);
    int x = posX;
    int y = posY;

    char asciiCar('a');
    char tp('a');
    char displayChar('Z');


    std::string displayString("");

    goTo(x, y);

    for(int i = 0; i < 2*taille + 2; i++){
        for(int j = 0; j < 2*taille + 2; j++){
            // 1�re ligne
            if(i == 0){
                if(j == 0) {
                    std::cout << "  ";
                }
                // Colonne paire, indice de colonne
                if(j % 2 == 0 && j >= 2){
                    // 1 chiffre
                    if(j/2 + 1 < 12)
                        std::cout << " " << j/2 - 1;
                    // sinon 2 chiffres
                    else
                        std::cout << j/2 - 1;
                }
                // Colonne impaire, barre verticale
                if(j % 2 == 1){
                    std::cout << "|";
                }
            }
            // Lignes suivantes
            else {
                // 1�re colonne
                if(j == 0 && i > 0){
                    // Ligne paire, indice lettre de ligne
                    if(i % 2 == 0){
                        tp = asciiCar + i/2 - 1;
                        std::cout << " " << tp;
                        //i/2-1 < 10 ? std::cout << " " << i/2 - 1 : std::cout << i/2 - 1;
                    }
                    // Ligne impaire, espace
                    if(i % 2 == 1){
                        std::cout << "  ";
                    }
                }
                // Autres colonnes
                else {
                    if(i % 2 == 1 && j % 2 == 0){
                        std::cout << "--";
                    }
                    if(j % 2 == 1){
                        if(i % 2 == 0){
                            std::cout << "|";
                            /// CASE A AFFICHER ETAIT ICI
                            std::cout << "  ";
                        }
                        else{
                            std::cout << "|";
                        }
                    }
                }
            }
        }
        goTo(x+i+1, y);
    }

    /// METTRE LA CASE A AFFICHER ICI
    for(size_t i = 0; i < p1.m_fleet.size(); i++){
        for(size_t j = 0; j < p1.m_fleet[i]->cases.size(); j++){
            x = p1.m_fleet[i]->cases[j].getX();
            y = p1.m_fleet[i]->cases[j].getY();

            goTo((x+1) * 2, (y+1) * 3);

            // La case contient un bateau
            displayChar = p1.m_fleet[i]->getDisplayChar();

            displayString.erase();

            for (size_t t = 0; t < 2; t++){
                displayString.push_back(displayChar);
            }

            switch(displayChar){
            case 'D':
                writeStringWithCol(displayString, 3, 0);
                break;
            case 'B':
                writeStringWithCol(displayString, 5, 0);
                break;
            case 'S':
                writeStringWithCol(displayString, 7, 0);
                break;
            case 'C':
                writeStringWithCol(displayString, 9, 0);
                break;
            }

        }
    }
    for(size_t i = 0; i < p2.m_fleet.size(); i++){
        for(size_t j = 0; j < p2.m_fleet[i]->cases.size(); j++){
            if(p2.m_fleet[i]->isHit[j]) {
                x = p2.m_fleet[i]->cases[j].getX();
                y = p2.m_fleet[i]->cases[j].getY();

                goTo((x+1) * 2, (y+1) * 3 + 60);
            }
        }
    }
}

void Grid::generateShips(Player& p)
{
    //goTo(36,0);
    //Cr�ation de 1 cuirass�
    for(size_t i = 0; i < 1 ; i++){
        p.m_fleet.push_back(new Battleship());
        do{
            createShip(p.m_fleet[i], 3);
        }while(collision(p, i));
    }

    //Cr�ation de 2 croiseurs
    for(int i = 0; i < 2 ; i++){
        p.m_fleet.push_back(new Cruiser());
        do{
            createShip(p.m_fleet[i+1], 2); //i+1 car d�ja 1 cuirass�
        }while(collision(p, i+1));
    }

    //Cr�ation de 3 destroyers
    for(int i = 0; i < 3 ; i++){
        p.m_fleet.push_back(new Destroyer());
        do{
            createShip(p.m_fleet[i+3], 1); //i+3 car d�ja 1 cuirass� et 2 croiseurs
        }while(collision(p, i+3));
    }

    //Cr�ation de 4 sous marins
    for(int i = 0; i < 4 ; i++){
        p.m_fleet.push_back(new Submarine());
        do{
            createShip(p.m_fleet[i+6], 0); //i+6 car d�ja 1 cuirass�, 2 croiseurs et 3 destroyers
        }while(collision(p, i+6));
    }

    /*//Affichage des coordonn�es des bateaux pour tester
    for(size_t i = 0 ; i < p.m_fleet.size() ; i++){
        for (size_t j = 0 ; j < p.m_fleet[i]->cases.size() ; j++){
            std::cout << "Bateau " << i+1 << " : " << p.m_fleet[i]->cases[j].getX() << " " << p.m_fleet[i]->cases[j].getY() << std::endl;
        }
        std::cout << std::endl;
    }*/
}

void Grid::createShip(Ship* s, int length)
{
    s->isHit.clear();
    s->cases.clear();

    s->setDirection(rand()%4);
    s->cases.push_back(Coord());
    for (int i = 1 ; i < length+1 ; i++)
    {
        if(s->getDirection() == 0 || s->getDirection() == 1)
        {
            s->cases.push_back(Coord(s->cases[0].getX()-i, s->cases[0].getY()));
            s->cases.push_back(Coord(s->cases[0].getX()+i, s->cases[0].getY()));
        }
        else if (s->getDirection() == 2 || s->getDirection() == 3){
            s->cases.push_back(Coord(s->cases[0].getX(), s->cases[0].getY()-i));
            s->cases.push_back(Coord(s->cases[0].getX(), s->cases[0].getY()+i));
        }
    }
    for (size_t i = 0 ; i < (2*length+1) ; i++)
    {
        s->isHit.push_back(false);
    }
}

bool Grid::collision(Player& p, unsigned int x)
{
    bool colliGrid = true;
    bool colliShip = true;
    for (size_t i = 0 ; i < p.m_fleet[x]->cases.size() ; i++)
    {
        colliGrid = collisionGrid(p.m_fleet[x]->cases[i]);
        if (colliGrid)
            return true;
        else
        {
            colliShip = collisionShip(p, p.m_fleet[x]->cases[i], x);
            if (colliShip)
                return true;
        }
    }
    return false;
}

bool Grid::collisionGrid(Coord cd)
{
        if (cd.getX() < 0 || cd.getX() > 14 || cd.getY() < 0 || cd.getY() > 14)
        {
            return true;
        }

    return false;
}

bool Grid::collisionShip(Player& p, Coord cd, unsigned int x)
{
    for (size_t i = 0 ; i < p.m_fleet.size() ; i++)
    {
        for (size_t j = 0 ; j < p.m_fleet[i]->cases.size() ; j++)
        {
            if(i != x)
            {
                if ((p.m_fleet[i]->cases[j].getX() == cd.getX()) && (p.m_fleet[i]->cases[j].getY() == cd.getY()))
                    return true;
            }
        }
    }
    return false;
}
