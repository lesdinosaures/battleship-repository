#include "Cruiser.h"

Cruiser::Cruiser()
 //       ::m_size(5), m_firePower(4)
{
    //ctor
    m_displayChar = 'C';
}

Cruiser::~Cruiser()
{
    //dtor
}

std::vector<Coord> Cruiser::whatIsHit(Coord cd)
{
    std::vector<Coord> hitBox;
    hitBox.push_back(cd);
    hitBox.push_back(Coord(cd.getX(), cd.getY()+1));
    hitBox.push_back(Coord(cd.getX()+1, cd.getY()+1));
    hitBox.push_back(Coord(cd.getX()+1, cd.getY()));
    return hitBox;
}
