#include "Player.h"

Player::Player()
{
    //ctor
}

Player::~Player()
{
    //dtor
}

bool Player::ableToMove(int rang)
{
    bool test;
    switch(m_fleet[rang]->getDirection())
    {
    case 0:
        for (size_t i = 0 ; i < m_fleet[rang]->cases.size() ; i++)
        {
            test = collis(rang, m_fleet[rang]->cases[i].getX()-1, m_fleet[rang]->cases[i].getY());
            if(test)
                return false;
        }
        m_fleet[rang]->moveForward();
        break;
    case 1:
        for (size_t i = 0 ; i < m_fleet[rang]->cases.size() ; i++)
        {
            test = collis(rang, m_fleet[rang]->cases[i].getX()+1, m_fleet[rang]->cases[i].getY());
            if(test)
                return false;
        }
         m_fleet[rang]->moveForward();
        break;
    case 2:
        for (size_t i = 0 ; i < m_fleet[rang]->cases.size() ; i++)
        {
            test = collis(rang, m_fleet[rang]->cases[i].getX(), m_fleet[rang]->cases[i].getY()+1);
            if(test)
                return false;
        }
         m_fleet[rang]->moveForward();
        break;
    case 3:
        for (size_t i = 0 ; i < m_fleet[rang]->cases.size() ; i++)
        {
            test = collis(rang, m_fleet[rang]->cases[i].getX(), m_fleet[rang]->cases[i].getY()-1);
            if(test)
                return false;
        }
         m_fleet[rang]->moveForward();
        break;
    }
    return true;
}

bool Player::ableToTurn(int rang)
{
    bool test;
    if ((m_fleet[rang]->getDirection() == 0) || (m_fleet[rang]->getDirection() == 1))
    {
        for (size_t i = 1 ; i < m_fleet[rang]->cases.size() ; i++) //On commence � 1 car pas besoin de tester la case centrale
        {
            if ((i == 1))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()+1, m_fleet[rang]->cases[i].getY()+1);
                if (test)
                    return false;
            }
            if ((i == 2))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()-1, m_fleet[rang]->cases[i].getY()-1);
                if (test)
                    return false;
            }
            if ((i == 3))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()+2, m_fleet[rang]->cases[i].getY()+2);
                if (test)
                    return false;
            }
            if ((i == 4))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()-2, m_fleet[rang]->cases[i].getY()-2);
                if (test)
                    return false;
            }
            if ((i == 5))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()+3, m_fleet[rang]->cases[i].getY()+3);
                if (test)
                    return false;
            }
            if ((i == 6))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()-3, m_fleet[rang]->cases[i].getY()-3);
                if (test)
                    return false;
            }
        }

    }
    else if ((m_fleet[rang]->getDirection() == 2) || (m_fleet[rang]->getDirection() == 3))
    {
        for (size_t i = 1 ; i < m_fleet[rang]->cases.size() ; i++) //On commence � 1 car pas besoin de tester la case centrale
        {
            if ((i == 1))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()-1, m_fleet[rang]->cases[i].getY()+1);
                if ((test))
                    return false;
            }
            if ((i == 2))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()+1, m_fleet[rang]->cases[i].getY()-1);
                if (test)
                    return false;
            }
            if ((i == 3))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()-2, m_fleet[rang]->cases[i].getY()+2);
                if (test)
                    return false;
            }
            if ((i == 4))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()+2, m_fleet[rang]->cases[i].getY()-2);
                if (test)
                    return false;
            }
            if ((i == 5))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()-3, m_fleet[rang]->cases[i].getY()+3);
                if (test)
                    return false;
            }
            if ((i == 6))
            {
                test = collis(rang, m_fleet[rang]->cases[i].getX()+3, m_fleet[rang]->cases[i].getY()-3);
                if (test)
                    return false;
            }
        }
    }
    else
    {
        std::cout << "erreur" << std::endl;
    }
    m_fleet[rang]->turn();
    return true;
}

bool Player::isShotSuccessful(std::vector<Coord> hitBox)
{
    bool shot;
    for (size_t i = 0 ; i < m_fleet.size() ; i++)
    {
        for (size_t j = 0 ; i < m_fleet[i]->cases.size() ; j++)
        {
            for (size_t k = 0 ; k < hitBox.size() ; k++)
            {
                if ((m_fleet[i]->cases[j].getX() == hitBox[k].getX()) && (m_fleet[i]->cases[j].getY() == hitBox[k].getY()))
                {
                    shot = true;
                    m_fleet[i]->wasShot(hitBox[k]);
                    if(m_fleet[i]->isShipSunk())
                    {
                        m_fleet.erase(m_fleet.begin()+i);
                        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        //Rajouter un std::cout du type "Bateau Coul�
                    }


                }
            }
        }
    }
    if(shot)
        return true;
    else
        return false;
}

bool Player::collis(int rang, int x, int y)
{
    bool collGrid;
    bool collShip;
    collGrid = collisGrid(x, y);
    if (collGrid)
        return true;
    else
    {
        collShip = collisShip(rang, x, y);
        if (collShip)
            return true;
    }
    return false;
}

bool Player::collisGrid(int x, int y)
{
    if (x < 0 || x > 14 || y < 0 || y > 14)
        return true;
    return false;
}

bool Player::collisShip(int rang, int x, int y)
{
    for (size_t i = 0 ; i < m_fleet.size() ; i++)
    {
        for (size_t j = 0 ; j < m_fleet[i]->cases.size() ; j++)
        {
            if(i != rang)
                if((m_fleet[i]->cases[j].getX() == x) && (m_fleet[i]->cases[j].getY() == y))
                    return true;
        }
    }
    return false;
}
