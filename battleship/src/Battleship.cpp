#include "Battleship.h"

Battleship::Battleship()
//           :m_size(7), m_firePower(9)
{
    //ctor
    m_displayChar = 'B';
}

Battleship::~Battleship()
{
    //dtor
}

std::vector<Coord> Battleship::whatIsHit(Coord cd)
{
    std::vector<Coord> hitBox;
    hitBox.push_back(cd);
    hitBox.push_back(Coord(cd.getX()-1, cd.getY()-1));
    hitBox.push_back(Coord(cd.getX()-1, cd.getY()));
    hitBox.push_back(Coord(cd.getX()-1, cd.getY()+1));
    hitBox.push_back(Coord(cd.getX(), cd.getY()+1));
    hitBox.push_back(Coord(cd.getX()+1, cd.getY()+1));
    hitBox.push_back(Coord(cd.getX()+1, cd.getY()));
    hitBox.push_back(Coord(cd.getX()+1, cd.getY()-1));
    hitBox.push_back(Coord(cd.getX(), cd.getY()-1));
    return hitBox;
}
