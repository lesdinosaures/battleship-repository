#include <iostream>

#include <stdlib.h>

#include "Menu.h"

Menu::Menu()
    :m_choice(0), m_choice2(0), m_quit(false), m_revenir_menu(false)
{

}

Menu::~Menu()
{

}

void Menu::showHelp()
{

}

void Menu::showCommands()
{

}

int Menu::show()
{
    while(m_choice<=0 || m_choice>4){
        system("cls");
        std::cout << "===========================================" << std::endl;
        std::cout << "========== MENU BATAILLE NAVALE ==========" << std::endl;
        std::cout << "===========================================" << std::endl << std::endl;
        std::cout << "1. JOUER UNE PARTIE" << std::endl;
        std::cout << "2. CHARGER UNE PARTIE" << std::endl;
        std::cout << "3. AIDE (commande d'utilisation et r�gle du jeu)" << std::endl;
        std::cout << "4. QUITTER " << std::endl;
        std::cout << " Veuillez entrer votre choix : " << std::endl;
        std::cin >> m_choice;
        std::cin.ignore(100, '\n');
        std::cout << std::endl;
    }
    switch(m_choice){
    case 1:
        system("cls");
        launchNewGame();
        break;
    case 2:
        system("cls");
        this->loadGame();
        break;
    case 3:
        system("cls");
        this->show2();
        break;
    case 4:
        m_quit = true;
        break;
    }

    return m_choice;
}

void Menu::launchNewGame(){
    // Launch new game
    Grid g;
    Player p1, p2;

    g.generateShips(p1);
    g.generateShips(p2);

    gameLoop(p1, p2, g);
}

void Menu::loadGame(){
    // generer le chargement
    Grid g;
    Player p1, p2;

    gameLoop(p1, p2, g);
}

void Menu::gameLoop(Player& p1, Player& p2, Grid g){
    // Game loop
    while(!GetAsyncKeyState(VK_ESCAPE)){
        // Tour du joueur 1
        // Boucle o� on peut se d�placer
        g.update(p1, p2);

        // Tour du joueur 2
        g.update(p2, p1);
    }
}

void Menu::show2()
{
    while (m_revenir_menu!= true){
        while (m_choice2 == 0 || m_choice2 > 4){
            // 2nd menu
            system("cls");
            std::cout << "===========================================" << std::endl;
            std::cout << "==========  MENU BATAILLE NAVALE ==========" << std::endl;
            std::cout << "======INSTRUCTIONS OU COMMANDE DE JEU======" << std::endl ;
            std::cout << "===========================================" << std::endl;
            std::cout << "1. INSTRUCTION DE JEU" << std::endl;
            std::cout << "2. COMMANDE DE JEU" << std::endl;
            std::cout << "3. REVENIR AU MENU" << std::endl;
            std::cout << "veuillez entrez votre choix :   " << std::endl;
            std::cin >> m_choice2;
            std::cin.ignore(100);
            std::cout << std::endl;
        }

        switch (m_choice2){
        case 1:
                system("cls");
                this->showHelp();
                break;
        case 2:
                system("cls");
                this->showCommands();
                break;
        case 3 :
                system("cls");
                m_revenir_menu= true;
                break;
        }
    }
}

